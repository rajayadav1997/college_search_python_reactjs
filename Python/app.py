from flask import Flask, jsonify, send_from_directory, request
from flask_cors import CORS
from config import *
import datetime
import traceback
import json

from util import *

app = Flask(__name__, static_url_path='')
CORS(app)

global_class = set_global_data()
@app.route('/', methods=['GET'])
def home():
        return jsonify({"author":"Raja Yadav","mail_id":"rajayadav1997@yahoo.com"}),200


@app.route('/add_college', methods=['POST'])
def add_college():
    try:
        read_json().add_json(global_class,request.json)
        return jsonify({"msg":"success"}),200
    except Exception as e:
        traceback.print_exc()
        log.info(e)
        return jsonify({"msg":str(e)}),400

@app.route('/update_college', methods=['PUT'])
def update_college():
    try:
        read_json().update_json(global_class,request.json)
        return jsonify({"msg":"success"}),200
    except Exception as e:
        traceback.print_exc()
        log.info(e)
        return jsonify({"msg":str(e)}),400

@app.route('/get_all_college', methods=['GET'])
def get_all_college():
    try:
        return jsonify({"msg":(global_class.get_data())}),200
    except Exception as e:
        traceback.print_exc()
        log.info(e)
        return jsonify({"msg":str(e)}),400

@app.route('/delete_college', methods=['DELETE'])
def delete_college():
    try:
        read_json().delete_json(global_class, request.json)
        return jsonify({"msg": "success"}), 200
    except Exception as e:
        traceback.print_exc()
        log.info(e)
        return jsonify({"msg":str(e)}),400


@app.route('/get_ten_college', methods=['GET'])
def get_ten_college():
    try:
        return jsonify({"msg": global_class.get_data()[0:10]}), 200
    except Exception as e:
        traceback.print_exc()
        log.info(e)
        return jsonify({"msg":str(e)}),400


@app.route('/get_college', methods=['POST'])
def get_college():
    try:
        name = request.json['name']
        code = request.json['code']
        domain = request.json['domain']
        return read_json().college_lists(global_class,name,code,domain),200
    except Exception as e:
        traceback.print_exc()
        log.info(e)
        return jsonify({"msg":str(e)}),400


@app.route('/get_college_names', methods=['GET'])
def get_college_names():
    try:
        return jsonify({"msg": read_json().get_names(global_class) }), 200
    except Exception as e:
        traceback.print_exc()
        log.info(e)
        return jsonify({"msg":str(e)}),400

@app.route('/get_college_code', methods=['GET'])
def get_college_code():
    try:
        return jsonify({"msg": read_json().get_codes(global_class) }), 200
    except Exception as e:
        traceback.print_exc()
        log.info(e)
        return jsonify({"msg":str(e)}),400

@app.route('/get_college_domain_extension', methods=['GET'])
def get_college_domain_extension():
    try:
        return jsonify({"msg": read_json().get_domain_extension(global_class) }), 200
    except Exception as e:
        traceback.print_exc()
        log.info(e)
        return jsonify({"msg":str(e)}),400



if __name__ == "__main__":
    if read_json().read(global_class):
        app.secret_key = session_key
        app.run(host=flask_host, port=flask_port)