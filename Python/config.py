import logging

session_key='rajayadav#1997'
flask_host = "0.0.0.0"
flask_port = "5000"

file_name = 'college.json'

logging_type = "DEBUG"
LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(threadName)s - %(thread)d - %(asctime)s  - %(name)s - %(funcName)s - %(lineno)d - %(levelname)s - %(message)s'
        }
    },
    'handlers': {
        'debugfilehandler': {
            'level': logging_type,
            'class': 'logging.FileHandler',
            'filename': 'app.log',
            'formatter': 'default'
        },
        'consolehandler': {
            'level': logging_type,
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        }
    },
    'loggers': {
        'app': {
            'handlers': ['debugfilehandler', 'consolehandler'],
            'level': logging_type,
            'propogate': True,
        },
        'console': {
            'handlers': ['consolehandler'],
            'level': logging_type,
            'propogate': True
        }
    }
}
from logging.config import dictConfig
dictConfig(LOGGING_CONFIG)
log = logging.getLogger('app')








class set_global_data():
    def __init__(self):
        self.college_data = []

    def set_data(self,data):
        self.college_data = data

    def get_data(self):
        return self.college_data