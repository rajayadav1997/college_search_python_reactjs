from config import *
import json
from flask import jsonify
import traceback
class read_json():
    def read(self,global_class):
        try:
            log.info("we are reading json")
            global_class.set_data([])
            with open(file_name) as f:
                log.info("data found in json")
                global_class.set_data(json.load(f))
            log.info("reading done")
            return True
        except Exception as e:
            traceback.print_exc()
            log.error(f"error in reading json {e}")
            return False

    def save_data(self,global_class):
        try:
            file_object = open(file_name, 'w')
            file_object.write(json.dumps(global_class.get_data()))
            file_object.close()
            return True
        except Exception as e:
            traceback.print_exc()
            log.error(f"error in writing json {e}")
            return False

    def add_json(self,global_class,data):
        try:
            global_json = global_class.get_data()
            for college in global_json:
                if((college['alpha_two_code'] ==data['alpha_two_code']) and (college['country'] ==data['country']) and
                        (college['domain'] ==data['domain']) and (college['name'] ==data['name'])
                        and (college['web_page'] ==data['web_page'])):
                    return False
            global_json.append(data)
            self.save_data(global_class)

        except Exception as e:
            traceback.print_exc()
            log.error(f"error in setting json {e}")
            return False


    def update_json(self,global_class,data):
        try:
            global_json = global_class.get_data()
            for college in global_json:
                if(college[data['key_for_condition']] == data['value_for_condition']):
                    new_data = data['data']
                    college['alpha_two_code'] = new_data['alpha_two_code']
                    college['country'] = new_data['country']
                    college['domain'] = new_data['domain']
                    college['name'] = new_data['name']
                    college['web_page'] =  new_data['web_page']

            self.save_data(global_class)

        except Exception as e:
            traceback.print_exc()
            log.error(f"error in setting json {e}")
            return False


    def delete_json(self,global_class,data):
        try:
            global_json = global_class.get_data()
            for college in global_json:
                if((college['alpha_two_code'] == data['alpha_two_code']) and (college['country'] ==data['country']) and
                        (college['domain'] ==data['domain']) and (college['name'] ==data['name'])
                        and (college['web_page'] ==data['web_page'])):

                    global_json.remove(data)

            self.save_data(global_class)

        except Exception as e:
            traceback.print_exc()
            log.error(f"error in deleting college {e}")
            return False




    def get_names(self,global_class):
        new_list = []
        try:
            global_json = global_class.get_data()
            for college in global_json:
                if college['name'] not in new_list:
                    new_list.append(college['name'])
        except Exception as e:
            traceback.print_exc()
            log.error(f"error in getting name {e}")
        return new_list


    def get_codes(self,global_class):
        new_list = []
        try:
            global_json = global_class.get_data()
            for college in global_json:
                if college['alpha_two_code'] not in new_list:
                    new_list.append(college['alpha_two_code'])
        except Exception as e:
            traceback.print_exc()
            log.error(f"error in getting alpha_two_code {e}")
        return new_list

    def get_domain_extension(self,global_class):
        new_list = []
        try:
            global_json = global_class.get_data()
            for college in global_json:
                if college['domain'].split('.')[1] not in new_list:
                    new_list.append(college['domain'].split('.')[1])
        except Exception as e:
            traceback.print_exc()
            log.error(f"error in getting domain {e}")
        return new_list


    def college_lists(self,global_class,name,code,domain):
        try:
            if(name =="") and (code =="") and (domain ==""):
                return jsonify({"msg": global_class.get_data()[0:10]})
            else:
                final_data = []
                global_json = global_class.get_data()
                for college in global_json:
                    dom_extention =college['domain'].split('.')[1]
                    if (name != "") and (college['name'] != name):
                        continue
                    if (code != "") and (college['code'] != code):
                        continue
                    if (domain != "") and (dom_extention != domain):
                        continue
                    final_data.append(college)

                return  jsonify({"msg": final_data})
        except Exception as e:
            traceback.print_exc()
            log.error(f"error in getting lists {e}")
            return jsonify({"msg": []})