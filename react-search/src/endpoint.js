const server = "http://localhost:5000";

export const endpoints = {
    get_college_code: server + "/get_college_code",
    get_college_domain_extension: server + "/get_college_domain_extension",
    get_college_names: server + "/get_college_names",
};
