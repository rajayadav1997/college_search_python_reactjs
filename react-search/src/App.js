import logo from './logo.svg';
import './App.css';
import { Helmet } from "react-helmet";
import React, { useState, useEffect }  from "react";
import {endpoints} from './endpoint';



 
function App() {
  const [country_code, setcountry_code] = useState([]);
  const [domain_list, setdomain_list] = useState([]);
  const [college_name, setcollege_name] = useState([]);

    const get_country_code = async () =>{      
      let data = null;   
          let res = await fetch(endpoints.get_college_code, {
              method: "GET",
              headers: {
                  "Content-Type": "application/json",
              }          
          }).catch((e) => {
              console.log(e);
          });
          data = await res.json();
          console.log('code');
          setcountry_code(data['msg']);
    };

    const get_domain_list = async () =>{      
      let data = null;   
          let res = await fetch(endpoints.get_college_domain_extension, {
              method: "GET",
              headers: {
                  "Content-Type": "application/json",
              }          
          }).catch((e) => {
              console.log(e);
          });
          data = await res.json();
          console.log('domain');
          setdomain_list(data['msg']);
    };

    const get_college_name = async () =>{      
      let data = null;   
          let res = await fetch(endpoints.get_college_names, {
              method: "GET",
              headers: {
                  "Content-Type": "application/json",
              }          
          }).catch((e) => {
              console.log(e);
          });
          data = await res.json();
          console.log('name');
          setcollege_name(data['msg']);
    };

  useEffect(() => {
    get_country_code();
    console.log('adadada');
    get_domain_list();
    get_college_name();
  }, []);
  return (
    <div className="s002">
      
      <form>
        <div className="inner-form">
        <input  type="text"  class="form-control" id="search-top" list="college_name" placeholder="What are you looking for?" />
        <datalist id="college_name">  
                    {college_name.map((res) => (
                      <option value={res}/>
                      ))}        
          </datalist>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <select data-trigger="" name="choices-single-defaul" id="select-top" class="form-control" >
			        <option placeholder="">Country Code</option>
                    {country_code.map((res) => (
                      <option value={res}>{res}</option>
                      ))}              
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <select data-trigger="" name="choices-single-defaul"  id="select-top" class="form-control" >
			            <option placeholder="">All Domain</option>
                  {domain_list.map((res) => (
                      <option value={res}>{res}</option>
                      ))}        
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button className="btn-search" class="btn btn-primary" id="button-top" type="button">SEARCH</button>
         
        </div>
      </form>
          <br></br>
            <div id="card-list">

            </div>
            <br></br>
            <div id="card-list">

        </div>

    </div>

  );
}

export default App;
